@extends('layouts.root')
@section('main')
<div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive py-4">
          <table class="table align-items-center table-flush" id="datatable">
            <thead class="thead-light">
              <tr>
                <th>No</th>
                <th>Product title</th>
                <th>Style</th>
                <th>Available size</th>
              </tr>
            </thead>
            <tbody class="my-data">
              @foreach($data as $value)
              <tr>
                  <td>{{ $loop->iteration }}</td>
                  <td>{{ $value->product_title }}</td>
                  <td>{{ $value->style }}</td>
                  <td>{{ $value->available_size }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('script')
<script>
$(document).ready(function(){
	$('#datatable').DataTable({
		"pageLength": 10,
		"lengthChange": false,
		"paging": true,
		"searching": true
	});
});
</script>
@endpush