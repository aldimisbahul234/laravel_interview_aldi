 <!-- Collapse -->
 <div class="collapse navbar-collapse" id="sidenav-collapse-main">
    <!-- Nav items -->
    <ul class="navbar-nav">
      <li class="nav-item">
         <a class="nav-link active" href="../../pages/widgets.html">
           <i class="ni ni-shop text-primary"></i>
           <span class="nav-link-text"> Dashboard</span>
         </a>
       </li>
       <li class="nav-item">
        <a class="nav-link" href="{{ url('product') }}">
          <i class="ni ni-ungroup"></i>
          <span class="nav-link-text">Product</span>
        </a>
      </li>
      {{-- 
      <li class="nav-item">
        <a class="nav-link" href="#navbar-examples" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-examples">
          <i class="ni ni-ungroup text-orange"></i>
          <span class="nav-link-text">System</span>
        </a>
        <div class="collapse" id="navbar-examples">
          <ul class="nav nav-sm flex-column">
            <li class="nav-item">
              <a href="../../pages/examples/pricing.html" class="nav-link">Pricing</a>
            </li>
            <li class="nav-item">
              <a href="../../pages/examples/login.html" class="nav-link">Login</a>
            </li>
          </ul>
        </div> --}}
      {{-- </li> --}}
    </ul>
    <!-- Divider -->
  </div>