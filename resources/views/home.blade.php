@extends('layouts.root')
@section('main')
   
<div class="row">
  <div class="col-lg-12".;,>
    <div class="card">
      <div class="card-body">
        <form action="{{ url('upload')}}" method="post" enctype="multipart/form-data">
          @csrf
        <div class="row">
          <div class="col-lg-10">
            <input class="form-control" type="file" name="upload_file">
          </div>
          <div class="col-lg-2">
            <button class="btn btn-primary"> UPLOAD</button>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>

  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive py-4">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th>Time</th>
                <th>File Name</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody class="my-data">
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</div>
 
@endsection
@push('script')
<script>
$(document).ready(function() {
  setInterval(function () {
          $.ajax({
                type: "GET",
                url: 'upload/get_data',
                data: {},
                success: function(data) {
                    $('.my-data').html(data)
                }
            });
        }, 1000);
})
</script>
@endpush