@extends('layouts.root')
@section('main')
   
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <form action="{{ url('upload')}}" method="post">
          @csrf
        <div class="row">
          <div class="col-lg-10">
            <input class="form-control" type="file">
          </div>
          <div class="col-lg-2">
            <button class="btn btn-primary"> UPLOAD</button>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>

  <div class="col-lg-12">
    <div class="card">
      <div class="card-body">
        <div class="table-responsive py-4">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th>Time</th>
                <th>File Name</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Time</td>
                <td>File Name</td>
                <td>Status</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</div>
 
@endsection
@push('script')
@endpush