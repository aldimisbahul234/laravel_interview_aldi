<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () { return redirect('login'); });



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth', 'namespace' => 'System'], function () {
    // Routing Group Menu Users
    Route::group(['prefix' => 'menu'], function () {
        Route::get('/', 'MenuController@index');
        Route::post('/', 'MenuController@store');
        Route::get('/create', 'MenuController@create');
        Route::get('/edit/{id}', 'MenuController@edit');
        Route::get('/detail/{id}', 'MenuController@profile');
        Route::post('/update/{id}', 'MenuController@update');
        Route::any('/delete/{id}', 'MenuController@delete');
    });
});
Route::group(['middleware' => 'auth', 'namespace' => 'Master'], function () {
    // Routing Group Menu Users
    Route::group(['prefix' => 'upload'], function () {
        Route::get('/', 'UploadController@index');
        Route::get('/get_data', 'UploadController@get_data');
        Route::post('/', 'UploadController@store');
        Route::get('/create', 'UploadController@create');
        Route::get('/edit/{id}', 'UploadController@edit');
        Route::get('/detail/{id}', 'UploadController@profile');
        Route::post('/update/{id}', 'UploadController@update');
        Route::any('/delete/{id}', 'UploadController@delete');
    });
    Route::group(['prefix' => 'product'], function () {
        Route::get('/', 'ProductController@index');
        Route::get('/get_data', 'ProductController@get_data');
        Route::post('/', 'ProductController@store');
        Route::get('/create', 'ProductController@create');
        Route::get('/edit/{id}', 'ProductController@edit');
        Route::get('/detail/{id}', 'ProductController@profile');
        Route::post('/update/{id}', 'ProductController@update');
        Route::any('/delete/{id}', 'ProductController@delete');
    });
});

