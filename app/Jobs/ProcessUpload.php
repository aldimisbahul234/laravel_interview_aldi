<?php

namespace App\Jobs;
use DB;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
class ProcessUpload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
  
    private $file_name = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file_name)
    {
        $this->file_name = $file_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file_name = $this->file_name;
        $get_file = storage_path('app/upload/'.$file_name);
        $spreadsheet = IOFactory::load($get_file);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $rows    = count($sheetData);
             for ($i=2; $i <= $rows; $i++) {
                $answers[] = [
                    'product_title'=>$sheetData[$i]['B'],
                    'product_description'=>$sheetData[$i]['C'],
                    'style'=>$sheetData[$i]['D'],
                    'available_size'=>$sheetData[$i]['E'],
                    'brand_logo_image'=>$sheetData[$i]['F'],
                    'thumbnail_image'=>$sheetData[$i]['G'],
                    'color_swatch_image'=>$sheetData[$i]['H'],
                    'product_image'=>$sheetData[$i]['I'],
                    'spec_sheet'=>$sheetData[$i]['J'],
                    'price_text'=>$sheetData[$i]['K'],
                    'suggested_price'=>$sheetData[$i]['L'],
                    'category_name'=>$sheetData[$i]['M'],
                    'sub_category'=>$sheetData[$i]['N'],
                    'color_name'=>$sheetData[$i]['O'],
                    'color_square_image'=>$sheetData[$i]['P'],
                    'color_product_image'=>$sheetData[$i]['Q'],
                    'color_product_image_thumbnail'=>$sheetData[$i]['R'],
                    'size'=>$sheetData[$i]['S'],
                    'qty'=>$sheetData[$i]['T'],
                    'piece_weight'=>$sheetData[$i]['U'],
                    'piece_price'=>$sheetData[$i]['P'],
                    'dozens_price'=>$sheetData[$i]['W'],
                    'cash_price'=>$sheetData[$i]['X'],
                    'price_group'=>$sheetData[$i]['Y'],
                    'case_size'=>$sheetData[$i]['Z'],
                    'inventory_key'=>$sheetData[$i]['AA'],
                    'size_index'=>$sheetData[$i]['AB'],
                    'sanmar_mainframe_color'=>$sheetData[$i]['AC'],
                    'mill'=>$sheetData[$i]['AD'],
                    'product_status'=>$sheetData[$i]['AE'],
                    'companion_styles'=>$sheetData[$i]['AF'],
                    'msrp'=>$sheetData[$i]['AG'],
                    'map_pricing'=>$sheetData[$i]['AH'],
                    'front_model_image_url'=>$sheetData[$i]['AI'],
                    'back_model_image'=>$sheetData[$i]['AJ'],
                    'front_flat_image'=>$sheetData[$i]['AK'],
                    'back_flat_image'=>$sheetData[$i]['AL'],
                    'product_measurements'=>$sheetData[$i]['AM'],
                    'pms_color'=>$sheetData[$i]['AN'],
                    'gtin'=>$sheetData[$i]['AO'],
                ];
            }
            try {

                foreach($answers as $value)
                {
                    DB::table('products')->insert($value);
                }
                DB::table('upload_status')->insert([
                    'status_upload'=>'success',
                    'filename'=>$file_name,
                    'created_at'=>Carbon::now()
                ]);
            } catch (\Exception $e) {
            
                DB::table('upload_status')->insert([
                    'status_upload'=>'failed',
                    'filename'=>$file_name,
                    'created_at'=>Carbon::now()
                ]);
            }
        
           
        

    }
}
