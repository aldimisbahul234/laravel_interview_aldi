<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessUpload;
use App\Http\Controllers\Import;
use Excel;
use DB;
use Illuminate\Support\Facades\Storage;
use App\User;
use Illuminate\Support\Facades\Hash;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use File;
class UploadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
       
       
       
        $tmp_file = $request->file('upload_file');
        $file_name =  $tmp_file->getClientOriginalName();

        $destinationPath = storage_path('app/upload');
        $request->file('upload_file')->move($destinationPath, $file_name);
        
        
       
       dispatch(new ProcessUpload($file_name));
       return redirect()->back();
    }
    public function get_data()
    {
        $data = DB::table('upload_status')->get();
        $html = '';
        foreach($data as $value){
            $html .='<tr>
                        <td>'.$value->created_at.'</td>
                        <td>'.$value->filename.'</td>
                        <td>'.$value->status_upload.'</td>
                        </tr>';
        }
        $html .='';
        return $html;
    }
}
