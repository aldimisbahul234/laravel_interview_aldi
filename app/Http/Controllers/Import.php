<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Jobs\ProcessUpload;
class Import implements ToModel
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
        dispatch(new ProcessUpload($row));
        // return new User([
        //    'name'     => $row[0],
        //    'email'    => $row[1],
        //    'password' => Hash::make($row[2]),
        // ]);
    }
}