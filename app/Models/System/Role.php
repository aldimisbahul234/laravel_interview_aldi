<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
use DB;
class Role extends Model
{
    protected $table = "sys_roles";
    protected $primaryKey = "role_id";
    protected $fillable = [
      'menu_id','division_id'
    ];
}
