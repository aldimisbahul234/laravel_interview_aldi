<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
use DB;
class Menu extends Model
{
    protected $table = "sys_menus";
    protected $primaryKey = "menu_id";
    protected $fillable = [
      'menu_name','menu_description','menu_icon','menu_status'
    ];
}
