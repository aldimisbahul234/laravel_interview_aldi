<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
use DB;
class Division extends Model
{
    protected $table = "sys_division";
    protected $primaryKey = "division_id";
    protected $fillable = [
      'division_name','division_status'
    ];
}
